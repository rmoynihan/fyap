from flask import Blueprint, render_template, request, url_for, redirect, flash
from datetime import datetime
import requests

from ..modules.api import BackendApi, ExternalApis
from ..modules.models import Post

# Create the blueprint object
delete_bp = Blueprint(
    "delete_bp", __name__, template_folder="../templates", static_folder="static"
)

api = BackendApi()
ext_api = ExternalApis()


@delete_bp.route("/delete_post/<post_id>")
def delete_post(post_id):
    """Delete a post"""

    #needs auth
    """ (claims, error_message) = verify_auth(request.cookies.get('token'))

    if claims == None or error_message != None:
        return redirect(url_for('auth.login'))

    user = User().init_args(claims['name'], claims['email'])
    user.find_account() """

    #check to see if the user owns he post
    """ if user._id != post['owner'] and user.permission == 'user':
        error = 'You do not have permission to delete this post!'
        return render_template('error.html.jinja2', error=error)
 """
    #find the post by id
    post = api.delete_post(post_id=post_id)

    # flash("You have successfully deleted this post")
    return redirect(url_for('feed_bp.feed'))
