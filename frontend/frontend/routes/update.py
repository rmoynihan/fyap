from flask import Blueprint, render_template, request, url_for, redirect
from datetime import datetime
import requests

from ..modules.api import BackendApi, ExternalApis
from ..modules.models import Post

# Create the blueprint object
update_bp = Blueprint(
    "update_bp", __name__, template_folder="../templates", static_folder="static"
)

api = BackendApi()
ext_api = ExternalApis()


@update_bp.route("/update-post/<post_id>", methods=["GET", "POST"])
def update_post(post_id):
    """Update a post and save changes"""

    # Retrieve the post information
    post = api.view_post(post_id)

    if request.method == "GET":
        post['tags'] = " ".join(post['tags'])
        return render_template('update_post.html.jinja2', post=post)

    else:
        # Get the form data
        post_data = request.form.to_dict()

        # Split the post's tags
        post_data['tags'] = post_data['tags'].split()

        # Create the post object
        # post = Post(**post_data)

        # Send the data to the backend
        response = api.update_post(post_data)
        print(response)

        return redirect(url_for('view_bp.view_post', post_id=response['id']))
