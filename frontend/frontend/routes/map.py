from flask import Blueprint, render_template, request, redirect, url_for
from ..modules.map import gmaps
from flask_googlemaps import Map, icons

from ..modules.api import BackendApi, ExternalApis
from ..modules.auth_func import verify_auth
from ..modules.filter import contruct_filters, breed_list

# Create the blueprint object
map_bp = Blueprint(
    "map_bp", __name__, template_folder="../templates", static_folder="static"
)

api = BackendApi()
ext_api = ExternalApis()

# Main account page
@map_bp.route("/", methods=["GET", "POST"])
def map():
    """Interactive map of dog spots in the area"""
    # check for authorization, redirect if not authorized
    check = verify_auth()

    if check is False:
        return redirect(url_for('auth_bp.login'))

    # Get user location
    user_lat, user_lng = ext_api.get_user_lat_lng(request)
    # user_lat, user_lng = ext_api.get_user_lat_lng(ip='107.128.183.164')

    filter_params = contruct_filters(request, user_lat, user_lng)

    print(filter_params)

    # Get posts with filter params (default or user-specified)
    posts = api.fetch_all_posts(filter_params)

    # Render the map
    icon = 'http://maps.google.com/mapfiles/ms/icons/blue-dot.png'
    posts_markers = ([{
        'icon': icon,
        'lat': p['loc'][1],
        'lng': p['loc'][0],
        'infobox': f"<b>{p['title']}</b><br /><img src='{p['image_url']}' />"
    } for p in posts])

    gmap = Map(
        identifier="gmap",
        varname="gmap",
        lat=user_lat,
        lng=user_lng,
        markers=posts_markers,
    )


    # Make tags into string
    # try:
    #     filter_params['tags'] = " ".join(filter_params['tags'])
    # except KeyError:
    #     pass

    return render_template("map.html.jinja2", gmap=gmap, filters=filter_params, breeds=breed_list)
