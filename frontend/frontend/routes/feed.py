from flask import Blueprint, render_template, request, redirect, url_for, session

from ..modules.api import BackendApi, ExternalApis
from ..modules.auth_func import verify_auth
from ..modules.filter import contruct_filters

# Create the blueprint object
feed_bp = Blueprint(
    "feed_bp", __name__, template_folder="../templates", static_folder="static"
)

api = BackendApi()
ext_api = ExternalApis()


# Main feed (effectively the homepage of the app)
@feed_bp.route("/", methods=["GET", "POST"])
def feed():
    """Get a list of all posts in the area that meet the specified criteria"""
    # check for authorization, redirect if not authorized
    check = verify_auth()

    if check is False:
        return redirect(url_for('auth_bp.login'))

    # Use the BackendApi class to fetch posts
    # user_lat, user_lng = ext_api.get_user_lat_lng('107.128.183.164')
    user_lat, user_lng = ext_api.get_user_lat_lng(request)

    # Get filter params
    filter_params = contruct_filters(request, user_lat, user_lng)

    # Fetch posts
    posts = api.fetch_all_posts(filter_params)

    # Render the feed view
    return render_template("feed.html.jinja2", posts=posts, user_id=session['user_id'])
