from flask import Blueprint, render_template, request, url_for, redirect, session
from datetime import datetime
import requests

from ..modules.api import BackendApi, ExternalApis
from ..modules.models import Post
from ..modules.auth_func import verify_auth

# Create the blueprint object
create_bp = Blueprint(
    "create_bp", __name__, template_folder="../templates", static_folder="static"
)

api = BackendApi()
ext_api = ExternalApis()


@create_bp.route("/", methods=["GET", "POST"])
def create_post():
    """Create a new post and upload it"""

    # check for authorization, redirect if not authorized
    check = verify_auth()

    if check is False:
        return redirect(url_for('auth_bp.login'))

    if request.method == "GET":
        return render_template('create_post.html.jinja2')

    else:
        # Get the form data
        post_data = request.form.to_dict()

        # Add a few attributes to post data
        post_data['user_id'] = session['user_id']
        post_data['timestamp'] = datetime.utcnow()
        post_data['tags'] = post_data['tags'].split()

        # Get user lat and lng
        lat, lng = ext_api.get_user_lat_lng(request)  # currently testing on localhost
        # lat, lng = ext_api.get_user_lat_lng('107.128.183.164')
        post_data['loc'] = [lng, lat]

        # Separate the filepath from the form data
        file = request.files['image_file'].read()

        # Create the post object
        post = Post(**post_data)

        # Send the data to the backend
        response = api.create_post(post, file)

        return redirect(url_for('feed_bp.feed'))

        #JSON decode error when try to do this
        #return redirect(url_for('view_bp.view_post', post_id=post_id))

        #return render_template(asdfasdf, post=post)
