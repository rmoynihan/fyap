from flask import Blueprint, render_template, session, request, redirect, url_for

import re

# from ..modules.auth_func import logout_user



# Create the blueprint object
auth_bp = Blueprint(
    "auth_bp", __name__, template_folder="../templates", static_folder="../static"
)


# Main account page
@auth_bp.route("/")
def login():
    """Present users with options to login, or create account/logout if not logged in"""

    return render_template("auth_index_new.html.jinja2")


@auth_bp.route('/logout')
def logout():
    """Logout user"""
    return render_template("logout.html.jinja2")

    # # Went with JS solution instead
    # success = logout_user()
    #
    # return redirect(url_for('feed_bp.feed'))
