from flask import Blueprint, render_template, request, url_for, redirect, session
from datetime import datetime
import requests

from ..modules.api import BackendApi, ExternalApis
from ..modules.models import Post

# Create the blueprint object
view_bp = Blueprint(
    "view_bp", __name__, template_folder="../templates", static_folder="static"
)

# Create api instance
api = BackendApi()


@view_bp.route("/view-post/<post_id>")
def view_post(post_id):
    """View a single post"""

    post = api.view_post(post_id=post_id)

    return render_template('view_post.html.jinja2', post=post, user_id=session['user_id'])

    # return redirect(url_for('view_bp.view_post', post_id=post_id))
