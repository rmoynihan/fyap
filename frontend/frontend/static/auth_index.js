// credit to: https://cloud.google.com/appengine/docs/standard/python3/building-app/authenticating-users
// create event listener for when the window is loaded
window.addEventListener('load', function () {
  // need to make a signout button or somethign for this to reference
  document.getElementByID('sign-out').onclick = function () {
    firebase.auth().signout();
    };

  // firebaseui config
  var uiConfig = {
    signInSuccessUrl: '/auth/',
    signInOptions: [
      // login options
      firebase.auth.GoogleAuthProvider.PROVIDER_ID,
      firebase.auth.EmailAuthProvider.PROVIDER_ID

    ],
    // terms of service url
    tosUrl: '<your-tos-url>'
  };

  // set up a function to create and or get the users idToken that will be used to call the users data
  firebase.auth().onAuthStateChanged(function (user) {
    if (user) {
      // the user has logged in *** this needs to be updated to redirect the user to the posts ***
      document.getElementByID('sign-out').hidden = false;
      console.log('Signed in as ${user.displayName} (${user.email})');
      user.getIdToken().then(function (token) {
        // add token to the browsers cookies so it can be called by mongodb
        document.cookie = 'token=' + token;
      });
    // create instance where user is signed out and redirect to the log in page
    } else {
      // show the login buttons/display
      var ui = new firebaseui.auth.AuthUI(firebase.auth());
      ui.start('#firebaseui-auth-container', uiConfig);
      print('this is being called')
      document.getElementByID('sign-out').hidden = true;
      // clear out the browser cookies
      document.cookie = 'token=';
    }
  // create error handler
  }, function (error) {
    console.log(error);
    alert('Unable to log in:' + error)
  });



});