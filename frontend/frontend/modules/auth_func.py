# https://github.com/APAD-Summer2020/SampleProject/blob/master/pet/modules/auth.py
import re

from flask import redirect, url_for, request, session

# import requests as requests_fh

from google.auth.transport import requests
import google.oauth2.id_token

def verify_auth():

    # set a id_token
    id_token = request.cookies.get('token')

    firebase_request_adapter = requests.Request()

    # Verify Firebase auth.
    error_message = None
    claims = None

    if id_token:
        try:
            claims = google.oauth2.id_token.verify_firebase_token(
                id_token, firebase_request_adapter)

            if 'email' not in claims:
                print('email not found\n', claims)
                error = 'FATAL ERROR! EMAIL NOT FOUND'
                return ([], error)

            if 'name' not in claims:
                regex = r'(.*)@(.*)\.(.*)'
                claims['name'] = re.match(regex, claims['email'])[1]
                print('name not found\n', claims)

        except ValueError as exc:
            # This will be raised if the token is expired or any other
            # verification checks fail.
            error_message = str(exc)

    # if user is not logged in
    if claims == None or error_message != None:
        return False
    # if user is logged in
    else:
        session['user_id'] = claims['user_id']
        return True

# # Does not do anything, went with JS script instead
# def logout_user():
#     print(request.cookies.get('token'))
#     response = requests_fh.post(
#         'https://accounts.google.com/o/oauth2/revoke',
#         params={'token': request.cookies.get('token')},
#         headers={'content-type': 'application/x-www-form-urlencoded'}
#     )
#     print('made request')
#     print(response.content)
#
#     return True
