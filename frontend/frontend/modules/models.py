from datetime import datetime
from typing import List, Optional
from pydantic import BaseModel, FilePath


class Post(BaseModel):
    """
    Schema for a Post object

    Use dictionary expansion to create initial
    Use Post.dict() method to
    """

    user_id: str
    id: Optional[str] = None
    timestamp: datetime
    image_url: Optional[str] = None
    title: str
    breed_primary: Optional[str] = None
    breed_secondary: Optional[str] = None
    tags: List[str]
    loc: List[float]


# Testing for Post object
if __name__ == '__main__':
    # 'Create Post' data
    data = {
        'user_id': 'ri13y',
        'timestamp': datetime.utcnow(),
        'title': 'Look at this boi!!!',
        'breed_primary': 'Golden Retriever',
        'breed_seconary': 'Poodle',
        'tags': ['Curly', 'Friendly', 'Big'],
        'loc': [-97.183, 30.312]
    }

    # Create the Post model object
    post = Post(**data)
    print(type(post))

    # 'Send' post to server
    # The server will handle assigning a post ID and image_url, this is just for demonstration purposes
    post.id = 'server-generated-id'
    post.image_url = 'http://server.generated.url'
    print(post.dict())
