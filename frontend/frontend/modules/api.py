import requests
import json


class BackendApi():
    """Class responsible for handling API calls to the backend"""

    def __init__(self, dev_mode=False):
        # Set base url
        if dev_mode:
            self.base_url = 'http://127.0.0.1:5001/'
        else:
            # self.base_url = 'http://127.0.0.1:5001/'
            self.base_url = 'https://backend-dot-team1-apad-project.wm.r.appspot.com/'


    def fetch_all_posts(self, filter_params):
        """Query the API for a list of posts meeting the specified filter criteria """

        payload = filter_params

        response = requests.get(self.base_url + '/posts/', params=payload)
        return response.json()['posts']


    def create_post(self, post, file):
        """Send a new post to the backend to be uploaded"""

        # Encode json in a somewhat convoluted way (can't have application/json headers when also sending a file)
        payload = {'json': post.json()}

        files = {
            'file': file
        }

        print(payload)

        response = requests.post(self.base_url + '/posts/create-post', data=payload, files=files)
        return response.json()


    def view_post(self, post_id):
        """View a single post"""

        response = requests.get(self.base_url + f'/posts/view-post/{post_id}')
        return response.json()


    def delete_post(self, post_id):
        """Delete a single post"""

        response = requests.get(self.base_url + f'/posts/delete-post/{post_id}')
        return response.json()


    def update_post(self, post_data):
        """Update a post"""

        # Encode json in a somewhat convoluted way (can't have application/json headers when also sending a file)
        payload = post_data

        response = requests.post(self.base_url + f'/posts/update-post/{post_data["id"]}', json=payload)
        return response.json()


class ExternalApis():
    """Class for making calls to any external APIs"""

    def get_user_lat_lng(self, request):

        if request.headers.getlist("X-Forwarded-For"):
            print("X Forwarded")
            ip = request.headers.getlist("X-Forwarded-For")[0]
        else:
            print("Remote addr")
            ip = request.remote_addr

        ip = '107.128.183.164'
        url = f'https://ipapi.co/{ip}/json'
        response = requests.get(url)
        response = response.json()

        try:
            return float(response['latitude']), float(response['longitude'])
        except TypeError:
            print("Resorting to hardcoded values")
            return 31.0, -97.0


if __name__ == "__main__":
    api = BackendApi()
    ext_api = ExternalApis()

    # Test code
    print(ext_api.get_user_lat_lng('104.57.191.173'))
