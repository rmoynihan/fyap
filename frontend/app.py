from flask import Flask, redirect, url_for
import os

# Set GCP service credentials
os.environ['GOOGLE_APPLICATION_CREDENTIALS'] = '../service_account_key.json'

# Import the blueprints
from frontend.routes.auth import auth_bp
from frontend.routes.feed import feed_bp
from frontend.routes.create import create_bp
from frontend.routes.update import update_bp
from frontend.routes.view import view_bp
from frontend.routes.delete import delete_bp
from frontend.routes.map import map_bp
from frontend.modules.map import gmaps


app = Flask(__name__)

# Register the blueprints
app.register_blueprint(auth_bp, url_prefix="/auth")
app.register_blueprint(feed_bp, url_prefix="/feed")
app.register_blueprint(create_bp, url_prefix="/create")
app.register_blueprint(update_bp, url_prefix="/update")
app.register_blueprint(view_bp, url_prefix="/view")
app.register_blueprint(delete_bp, url_prefix="/delete")
app.register_blueprint(map_bp, url_prefix="/map")

gmaps.init_app(app)

app.secret_key = 'fyap0112358'
app._static_folder = 'frontend/static/'

#Redirect from index to feed
@app.route("/")
def redirect_home():
    #TODO: CHANGE LINK BEFORE PUBLISHING
    # return redirect("http://127.0.0.1:5000/feed/", code=302)
    return redirect(url_for('feed_bp.feed'))



# Start the app
if __name__ == "__main__":
    app.run(debug=True)
