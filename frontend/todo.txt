FRONTEND TODO

So looking at Phase 1, basically all features of the web app except for the map overlay need to be complete.

From the frontend standpoint, this includes:
- Create account
- Login in
- View recent dog spots in an Instagram-style feed (this will be the homepage)
- Search recent dog spots
  - By breed (our "theme")
  - By tag ("fluffly", "big", "cuddly", etc.)
- Post a dog spot (for the web app, this will assume it is just uploading a photo along with the post, not taking a new photo via the browser)

We also might want options to edit/delete spots later on, but that can wait for now.

Each of these features will more than likely need a route and a view. I made some skeleton code for the authentication and feed modules using Flask Blueprints,
so you can keep following that structure pattern for additional Blueprints you need to make.
Keeping things as modular and separated as possible will help drastically as this thing gets bigger and more convoluted.

Now here's the nice thing: most of the actual logic for these functions will be handled by Denver and I in the backend.

For items like getting the main feed, all the frontend will do is make a HTTP GET request to the backend API we set up to retrieve a list of posts in JSON format,
and then format/display the posts in the html/jinja2 template.
For items like posting a new dog spot, it will be very similar - you'll make a form setup that allows the user to create the post with text/tags/photo, and then you'll
send the JSON data via an HTTP POST request to another endpoint we set up, and that's it.
Once Denver and I get the database stuff setup, we'll write up the template for how things are getting returned so y'all can handle it appropriately.

The exception will be with creating accounts and logging in. These will not go through the backend, so any authentication logic needs to be handled in the frontend.
Pay close attention to the App Engine HW5, especially the part about Firebase authentication, as that is the system we will be using.
However, I did find a nice Python wrapper that you can install that should make things more familiar than in that tutorial.


For version control, make sure to use branches and commit often.
Above all else, be liberal with comments!!!


Helpful links:
Blueprint tutorial Mary posted - https://realpython.com/flask-blueprint/
Python Requests module (for getting/recieve JSON data from our API) - https://realpython.com/python-requests/
Working with JSON in Python - https://www.w3schools.com/python/python_json.asp
Python wrapper for Firebase - https://github.com/thisbejim/Pyrebase
An example of the wrapper being used - https://github.com/kanuarj/FirebasePython/blob/master/Auth/app.py
Passing Flask data into JavaScript tags - https://medium.com/@crawftv/javascript-jinja-flask-b0ebfdb406b3
