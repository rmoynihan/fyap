### Team #1 APAD Summer Project
This is the summer 2020 APAD project for Team #1.

Team members:
 - Denver Baumgartner
 - Debleena Das
 - Riley Moynihan
 - Katherine Wroble

# Find You a Pupper!

<img src="http://4.bp.blogspot.com/-l4LeMkA-2ps/T-gHQ2feMtI/AAAAAAAJEEY/yRnW9RL8Zok/s320/Cute+Puppies+%281%29.jpg"
     alt="Markdown Monster icon"
     style="float: left; margin-right: 10px;" />

People encounter dogs all the time around the neighborhood, in the park, at the lake, and plenty of other outdoor places. And, often times, those dogs are heckin' adorable. But wouldn't it be great if you could show other people the dogs you see? Or even better, find out where other people are seeing some good boys themselves?

Find You a Pupper! is an application designed to crowdsource the act of spotting and finding cute dogs. People can upload pictures of dogs they spot out and about so that other users can know where cute dogs can be found. In addition, users can add tags describing the dogs they post, so that other users can filter the dogs around them to find a particular breed or characteristic they might be interested in. With Find You a Pupper!, any time you feel the need to see some puppers struttin' their stuff, you'll know exactly where to find them.

## Technical Details
This project is build using Flask as the webapp framework, Jinja2 as the templating engine, and Google App Engine / Firebase as the hosting solution. There is also an Android app component as well running on the same GCP project.

## Extra Features
We added the functionality to edit and delete a dog post. Our website recognizes users and the posts that they have added to the feed, and it uses that information to only show options to edit and delte posts that they have uploaded. Users that did not submit a given post, will only see the option to view the details of the post and will not be given the option to edit or delete that post.

On our home page, we have added a carousel feature that introduces users to the basic functions of our website. The images in this carousel can be updated over time to best fit the needs of the website, showcasing whatever information is necessary at that time.