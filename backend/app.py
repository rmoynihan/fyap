import os
from flask import Flask

from backend.modules.database import mongo

# Set GCP service credentials
os.environ['GOOGLE_APPLICATION_CREDENTIALS'] = '../service_account_key.json'

# Import the blueprints
from backend.services.posts import posts_bp

# Create app object
app = Flask(__name__)

app.config['MONGO_URI'] = "mongodb+srv://admin:adminPassword@fyap0.1czn2.gcp.mongodb.net/fyap-db?retryWrites=true&w=majority&ssl_cert_reqs=CERT_NONE"
mongo.init_app(app)

# Register the blueprints
app.register_blueprint(posts_bp, url_prefix="/posts")


# Start the app
if __name__ == "__main__":
    app.run(debug=True, port=5001)
