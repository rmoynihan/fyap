from flask import Blueprint, render_template, request, jsonify

from bson import json_util, ObjectId
from bson.son import SON
import json
import uuid
import os

from ..modules.database import mongo, firebase, storage
from ..modules import distance
from ..modules.image import ImageMutator

# Create the blueprint object
posts_bp = Blueprint("posts_bp", __name__)


# Main account page
@posts_bp.route("/")
def posts():
    """Return a list of posts with optional filters"""

    # Get user's lat and lng
    user_lat, user_lng = float(request.args.get('user_lat')), float(request.args.get('user_lng'))

    # Get the max range in lat and lng degrees a user wants to search in
    max_range_in_miles = int(request.args.get('max_radius'))

    # Geospatial range - only get posts within m number of miles
    query = {
        "loc": {"$nearSphere": {"$geometry": {"type": "Point", "coordinates": [user_lng, user_lat]}, "$maxDistance": max_range_in_miles * distance.METERS_IN_A_MILE}}
    }

    # Check for breed and tag query params
    breed_primary = request.args.get('breed_primary')
    breed_secondary = request.args.get('breed_secondary')
    tags = request.args.get('tags')

    if breed_primary is not None:
        query['breed_primary'] = breed_primary

    if breed_secondary is not None:
        query['breed_secondary'] = breed_secondary

    if tags is not None:
        tags = tags.split(' ')
        query["tags"] = {"$in": tags}

    print(query)

    ######################
    # Query the database #
    ######################

    # Execute query and get posts
    posts = list(mongo.db.posts.find(query).sort('_id'))
    for post in posts:
        post['id'] = str(post['_id'])

    # Return JSON
    return {'posts': json.loads(json_util.dumps(posts))}


# View single post
@posts_bp.route("/view-post/<post_id>")
def post(post_id):
    """View details about a single post"""

    # Query document
    single_post = mongo.db.posts.find({'_id': ObjectId(post_id)})

    # Return post as JSON data if it exists
    if single_post.count() > 0:
        single_post = list(single_post)[0]
        single_post['id'] = str(single_post['_id'])

        return json.loads(json_util.dumps(single_post))

    # If it doesn't exist, return post not found message and 403 status code
    else:
        return {'message':'post not found'}, 403



# Create a new post
@posts_bp.route("/create-post", methods=['POST'])
def create_post():
    """Create a new post"""

    # Get JSON data from request
    data = request.form.to_dict()
    post = json.loads(data['json'])

    if 'file' not in request.files:
        return {'message': 'error, no file uploaded'}, 400

    # Get the image and save it to tmp
    file = request.files['file']
    filename = str(uuid.uuid4())

    dirname = os.path.dirname(__file__)
    filepath = os.path.join(dirname, f'tmp/{filename}')

    try:
        file.save(filepath)
    except OSError:
        filepath = f'/tmp/{filename}'
        file.save(filepath)

    # Scale and crop image
    ImageMutator.scale_image(filepath)

    # Upload image to Firestore
    storage.child(f'images/{filename}').put(filepath)

    # Delete the tmp file
    os.remove(filepath)

    # Add the file url to the post data
    url = storage.child(f'images/{filename}').get_url(None)
    print(post)
    post['image_url'] = url

    # Create a new document with the JSON data
    mongo.db.posts.insert_one(post)

    # Return created post
    return json.loads(json_util.dumps(post))


# Update post
@posts_bp.route("/update-post/<post_id>", methods=['POST'])
def update_post(post_id):
    """Update a post"""

    # Get JSON data from request
    data = request.json

    # Set the document to the JSON data
    result = mongo.db.posts.update_one({'_id': ObjectId(post_id)}, {'$set': data})

    # Return post JSON data
    if result.matched_count > 0:
        return json.loads(json_util.dumps(data))

    # Post not found
    else:
        return {'message':'post not found'}, 403


# Delete post
@posts_bp.route("/delete-post/<post_id>")
def delete_post(post_id):
    """Delete a post"""

    # Find the post document
    did_delete = mongo.db.posts.delete_one({"_id": ObjectId(post_id)})

    # Delete the post
    if did_delete.deleted_count > 0:
        return {'message':'post deleted successfully'}

    # Post not found
    else:
        return {'message':'post not found'}, 403
