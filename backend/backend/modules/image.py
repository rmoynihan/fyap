from PIL import Image

class ImageMutator:
    @classmethod
    def scale_image(cls, image_path):
        NEW_WIDTH = 300
        NEW_HEIGHT = 200

        # Open image
        im = Image.open(image_path)

        # Apply the thumbnail transformation
        im.thumbnail((NEW_WIDTH, NEW_HEIGHT))

        try:
            im.save(image_path, "JPEG")
        except OSError:
            im.save(image_path, "PNG")
