from flask_pymongo import PyMongo
import json
from bson import ObjectId
from firebase import Firebase

# Mongo client
mongo = PyMongo()

config = {
  "apiKey": "AIzaSyDpWEmVsPnE0KVFCN9mN7jgF7vSiewSvK0",
  "authDomain": "team1-apad-project.firebaseapp.com",
  "databaseURL": "https://team1-apad-project.firebaseio.com",
  "projectId": "team1-apad-project",
  "storageBucket": "team1-apad-project.appspot.com"
}

firebase = Firebase(config)
storage = firebase.storage()

# # Custom JSON encoder for Mongo data
# class JSONEncoder(json.JSONEncoder):
#     def default(self, o):
#         if isistance(o, ObjectId):
#             return str(o)
#
#         return json.JSONEncoder.default(self, o)

if __name__ == '__main__':
    print(storage.child('images/70c7bc6f-4c96-4a6d-a101-6f74e77a2f35').get_url(None))
