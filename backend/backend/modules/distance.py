import math


# Set constants
MILES_IN_DEG_LAT = 69.172  # also longitude at equator conversion
METERS_IN_A_MILE = 1609.34


# # DEPRECATED - MongoDB has this built in
# def max_range_lat_long(max_range_in_miles, current_latitude):
#     """Converts max range in miles into max degrees difference of latitude and longitude"""
#
#     # Solve for legs of right isosceles
#     a = max_range_in_miles / math.sqrt(2)
#
#     # Convert latitude leg into degrees
#     a1 = a / MILES_IN_DEG_LAT
#
#     # Convert longitude leg into degrees
#     a2 = a / (math.cos(math.radians(abs(current_latitude))) * MILES_IN_DEG_LAT)
#
#     return a1, a2
