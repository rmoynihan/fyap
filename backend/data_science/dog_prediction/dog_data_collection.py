# file to collect all training data and pickle to be used later

# code to assist in collecting file names
# https://stackoverflow.com/questions/9816816/get-absolute-paths-of-all-files-in-a-directory

import os

import pickle

# create dog object
class dog():

    def __init__(self, breed, picPath, dogID, annotation):

        self.breed = breed
        self.picPath = picPath
        self.dogID = dogID
        self.annotation = annotation

# get the file paths for annotations
# https://stackoverflow.com/questions/9816816/get-absolute-paths-of-all-files-in-a-directory
'''
import collections
dogs = collections.defaultdict(list)

nameTest = 'Chihuahua'
index = 0
for root, dirs, files in os.walk(os.path.abspath("/Users/denverbaumgartner/Documents/Team1/backend/dog_data/Annotation")):
    for file in files:
        send = os.path.join(root, file)
        with open(send, 'r') as test:
            test_contents = str(test.read())
            folderS = test_contents.index('<folder>') + len('<folder>')
            folderE = test_contents.index('</folder>')

            fileS = test_contents.index('<filename>') + len('<filename>')
            fileE = test_contents.index('</filename>')

            nameS = test_contents.index('<name>') + len('<name>')
            nameE = test_contents.index('</name>')

            folder = test_contents[folderS: folderE]
            file = test_contents[fileS: fileE]
            name = test_contents[nameS: nameE]

            newPath = send.replace('Annotation', 'Images')
            picPath = newPath + '.jpg'

            #/ Users / denverbaumgartner / Documents / Team1 / backend / dog_data / Images / n02085620 - Chihuahua / n02085620_7.jpg
            dogID = picPath.split('/')[-2] + picPath.split('/')[-1]
            dogID = dogID[:-4]

            if name != nameTest:
                nameTest = name
                index += 1

            dogs[name].append(dog(name, picPath, dogID, index))

print(dogs['French_bulldog'][0].breed)
print(dogs['French_bulldog'][0].picPath)
print(dogs['French_bulldog'][0].dogID)
print(dogs['French_bulldog'][0].annotation)

print(dogs['French_bulldog'][1].breed)
print(dogs['French_bulldog'][1].picPath)
print(dogs['French_bulldog'][1].dogID)
print(dogs['French_bulldog'][1].annotation)

filename = 'dog_trainingData'
outfile = open(filename, 'wb')

pickle.dump(dogs, outfile)
outfile.close()

class dog_data():

    # create class variables to be used
    def __init__(self, annotation_path):

        # path for annotations
        self.annotation_path = annotation_path

        # dictionary to store the 

path = '/Users/denverbaumgartner/Documents/MSITM_Code/MIS_S385N/Team1/backend/dog_data/Annotation/n02085620-Chihuahua/n02085620_7'
# checked with https://stackoverflow.com/questions/23785249/what-is-a-attributeerror-io-textiowrapper-object-has-no-attribute-replace
with open(path, 'r') as test:
    test_contents = str(test.read())


find a better way to do this if possible 

# index the position of tags
folderS = test_contents.index('<folder>') + len('<folder>')
folderE = test_contents.index('</folder>')

fileS = test_contents.index('<filename>') + len('<filename>')
fileE = test_contents.index('</filename>')

nameS = test_contents.index('<name>') + len('<name>')
nameE = test_contents.index('</name>')

folder = test_contents[folderS: folderE]
file = test_contents[fileS: fileE]
name = test_contents[nameS: nameE]

print(folder)
print(file)
print(name)

picPath = '/Users/denverbaumgartner/Documents/MSITM_Code/MIS_S385N/Team1/backend/dog_data/Images/' + folder + '-' + name + '/' + file + '.jpg'
print(filePath)
'''