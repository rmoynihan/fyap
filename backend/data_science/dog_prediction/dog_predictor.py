# this is a tutorial example, the tutorial has been listed below for reference
# https://www.kaggle.com/anujverma126/dog-breed-classification-beginner-s-tutorial?

import os
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
# this function is only used in IPyhon
# https://stackoverflow.com/questions/43027980/purpose-of-matplotlib-inline
# %matplotlib inline
import seaborn as sns

import cv2
import random
from random import randint
import time


import torch
from torch.utils.data import Dataset, random_split, DataLoader
import torch.nn.functional as F
import torch.nn as nn

from PIL import Image
from scipy import ndimage

import torchvision
import torchvision.models as models
import torchvision.transforms as T
from torchvision.utils import make_grid
from torchvision.datasets.utils import download_url
from torchvision.datasets import ImageFolder

from tqdm.notebook import tqdm

from sklearn.metrics import f1_score

import pickle

# import the dog class
from dog_data_collection import dog
'''
# help from stackoverflow to handle .mat files
# https://stackoverflow.com/questions/874461/read-mat-files-in-python

import scipy.io

# open and convert the list files
file_list = scipy.io.loadmat('/Users/denverbaumgartner/Documents/MSITM_Code/MIS_S385N/Team1/backend/dog_data/lists/file_list.mat')
test_list = scipy.io.loadmat('/Users/denverbaumgartner/Documents/MSITM_Code/MIS_S385N/Team1/backend/dog_data/lists/test_list.mat')
train_list = scipy.io.loadmat('/Users/denverbaumgartner/Documents/MSITM_Code/MIS_S385N/Team1/backend/dog_data/lists/train_list.mat')

# open and convert test/train data
test_data = scipy.io.loadmat('/Users/denverbaumgartner/Documents/MSITM_Code/MIS_S385N/Team1/backend/dog_data/test_data.mat')
train_data = scipy.io.loadmat('/Users/denverbaumgartner/Documents/MSITM_Code/MIS_S385N/Team1/backend/dog_data/train_data.mat')

print(test_list.keys())
print('training list')
print(train_list.keys())
print(train_list['labels'])
print(train_list['annotation_list'])
'''

# get the dataset
infile = open('/Users/denverbaumgartner/Documents/Team1/backend/backend_bps/posts/dog_prediction/dog_trainingData','rb')
dogData = pickle.load(infile)
infile.close()

# set image standards
imagenet_stats = ([0.485, 0.456, 0.406], [0.229, 0.224, 0.225])

# create transformation standards
train_tfms = T.Compose([
    T.Resize((300,300)),
#    T.CenterCrop(256),
    T.ColorJitter(brightness=0.1, contrast=0.1, saturation=0.1, hue=0.1),
#    T.RandomCrop(32, padding=4, padding_mode='reflect'),
    T.RandomHorizontalFlip(),
    T.RandomRotation(10),
    T.ToTensor(),
    T.Normalize(*imagenet_stats,inplace=True),
#    T.RandomErasing(inplace=True)
])

valid_tfms = T.Compose([
    T.Resize((300,300)),
    #T.CenterCrop(256),
    T.ToTensor(),
    T.Normalize(*imagenet_stats)
])

# create class for dog
class DogDataset(Dataset):
    def __init__(self, df, transform=None):
        self.df = df
        self.transform = transform

    def __len__(self):
        return len(self.df)

    def __getitem__(self):

        img_id, img_label = df['breed'], row['annotation']
        img_fname = df['picPath']
        img = Image.open(img_fname)
        if self.transform:
            img = self.transform(img)
        return img, img_label

# create distribution of samples
np.random.seed(42)
msk = np.random.rand(len(dogData)) < 0.8
'''
# create training and indexes
train_df = dogData[msk].reset_index()
val_df = dogData[~msk].reset_index()

# create training and validation datasets
train_ds = DogDataset(train_df, TRAIN_DIR, transform=train_tfms)
val_ds = DogDataset(val_df, TRAIN_DIR, transform=valid_tfms)
len(train_ds), len(val_ds)
'''

